package com.demo.springboot.service;

import com.demo.springboot.dto.ResultDto;

public interface QuadraticFunction {

    ResultDto calculateFunction(Double a, Double b, Double c);
}
